using System.Threading.Tasks;
using si3miniproject.Models;

namespace si3miniproject.Repo
{
    public interface IMySqlRepo
    {
        Task AddRatingAsync(ratingModel ratingModel);    
    }
}