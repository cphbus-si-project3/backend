using System;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using si3miniproject.Models;

namespace si3miniproject.Repo
{
    public class MySqlRepo : IMySqlRepo
    {
        string connectionString;

        public MySqlRepo()
        {
            connectionString = Environment.GetEnvironmentVariable("cs");
        }

        public async Task AddRatingAsync(ratingModel ratingModel)
        {
            using (var conn = new MySqlConnection(connectionString))
            {   
                var sqlstr = @"INSERT INTO RatingTab(location, gender, age, rating, description) 
                            VALUES (@LOCATION, @GENDER, @AGE, @RATING, @DESCRIPTION)";

                var command = new MySqlCommand(sqlstr, conn);
                command.Parameters.Add("@LOCATION", MySqlDbType.String);
                command.Parameters["@LOCATION"].Value = ratingModel.Location;

                command.Parameters.Add("@GENDER", MySqlDbType.String);
                command.Parameters["@GENDER"].Value = ratingModel.Gender;

                command.Parameters.Add("@AGE", MySqlDbType.Int32);
                command.Parameters["@AGE"].Value = ratingModel.Age;

                command.Parameters.Add("@RATING", MySqlDbType.Int32);
                command.Parameters["@RATING"].Value = ratingModel.Rating;

                command.Parameters.Add("@DESCRIPTION", MySqlDbType.String);
                command.Parameters["@DESCRIPTION"].Value = ratingModel.Description;

                try
                {
                    await conn.OpenAsync();
                    await command.ExecuteNonQueryAsync();
                }
                catch (System.Exception ex)
                {
                    throw new Exception($"Database error : {ex.Message}");
                }
            }
        }
    }
}