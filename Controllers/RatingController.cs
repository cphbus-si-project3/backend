using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using si3miniproject.Models;
using si3miniproject.Repo;

namespace si3miniproject
{
    [ApiController]
    [Route("[controller]")]
    public class Ratingcontroller : Controller
    {
        private readonly IMySqlRepo ctx;

        public Ratingcontroller(IMySqlRepo ctx)
        {
            this.ctx = ctx;
        }

        [HttpPost]
        public async Task<IActionResult> Add(ratingModel ratingData){
            if(!ModelState.IsValid){
                return BadRequest("Modelstate error");
            }
            await ctx.AddRatingAsync(ratingData);
            return Ok();
        }

        [HttpGet]
        public IActionResult index(){
            return Ok("webapi running");
        }
    }
}