using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace si3miniproject.Models
{
    public class ratingModel
    {
        public string Location { get; set; }
        public string Gender { get; set; }
        public int Age { get; set; }
        [Required]
        public int Rating{ get; set; }
        [Required]
        public string Description { get; set; }
    }
}